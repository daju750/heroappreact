import React,{useContext} from "react";
import {BrowserRouter as Routers,Route,Switch} from 'react-router-dom'
import {LoginScreen} from "../LoginScreen";
import {DashboardRouters} from "./DashboardRouters";
import {PrivateRouter} from "../routers/PrivateRouter"
import {AuthConext} from "../../auth/AuthConext";
//import {PublicRoute} from "../routers/PublicRoute";

export const AppRouter = () => {

    const {user} = useContext(AuthConext)


    return (
        <>
        <Routers>
            <Switch>                
                <Route exact path="/Login" component={LoginScreen}/>
                <PrivateRouter path="/" component={DashboardRouters} isAuthenticated={user.logged}/>
            </Switch>
        </Routers>
        </>
    )
} 
