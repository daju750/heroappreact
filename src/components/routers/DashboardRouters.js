import React from "react";
import {Route, Switch} from 'react-router-dom'
import {NavBar} from "../ui/NavBar";
import {DcScreen} from "../DcScreen"
import {MarvelScreen} from "../MarvelScreen"
import {HomeScreen} from "../HomeScreen";
import {HeroeScreen} from "../HeroeScreen";

export const DashboardRouters = () => {
    
    return (
    <>
    <NavBar/>
        <div className="container mt-3">
            <Switch>
                <Route path="/Marvel" component={MarvelScreen}/>
                <Route path="/Dc" component={DcScreen}/>
                <Route path="/hero/:heroeId" component={HeroeScreen}/>
                <Route path="/" component={HomeScreen}/>
            </Switch>
        </div>
    </>
    )
}