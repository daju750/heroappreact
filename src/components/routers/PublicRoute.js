import React from 'react'
import {Route,Switch,Redirect} from 'react-router-dom'
import PropTypes from 'prop-types'

export const PublicRoute =({
    isAuthenticated,
    component:Component,
    ...rest
}) => {
    return (
    <>
        <Switch>
        <Route {...rest} 
            component = {(props) => ( 
                (isAuthenticated) ? (<Component {...props}/>) : (<Redirect to ="/Contact"/>) 
            )}
        />
        </Switch>
    </>
    )
}

PublicRoute.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired
}