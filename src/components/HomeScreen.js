import React, { useMemo } from 'react'
//import { heroes } from './data/heroes'
import {useForm} from './hook/useForm'
import queryString from 'query-string'
import { useLocation } from 'react-router-dom'
import { useHistory } from "react-router-dom";
import {getHeroesByName} from "./selectors/getHeroesByName"
import {HeroCard} from "./heroes/HeroCard"

export const HomeScreen = () => {

    let history = useHistory();

    const location = useLocation()
    const {q=''}=queryString.parse(location.search)

    const[formValues,handleInputChange] = useForm({
        searchText: q
    })

    const {searchText} = formValues

    const heroesFiltered = useMemo(()=>getHeroesByName(q),[q])
    console.log(heroesFiltered)

    const handleSearch = (e) => {
        e.preventDefault();
        history.push("?q="+searchText);
    }

    //{heroesFiltered.map(hero=>(<HeroCard key={hero.id}/>))}
    return(
        <div className='container'>
            <h1>Buscar Heroe</h1>
            <hr/>
            <div className='row'>
                <div className='col-5'>
                    <form onSubmit={handleSearch}>
                        <input type="text" placeholder='Find your hero' onChange={handleInputChange} autoComplete='off' name='searchText' className='form-control'/>
                        <button type='submit' className='btn m-1 btn-block btn-outline-primary'>Seach...</button>
                    </form>
                    <hr/>
                    {heroesFiltered.map(hero=>(<HeroCard key={hero.id} {...hero}/>))}
                </div>
            </div>
        </div>
    )
}