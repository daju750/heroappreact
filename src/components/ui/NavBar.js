import React, { useContext } from 'react'
import {Link} from 'react-router-dom'
import {NavLink} from 'react-router-dom'
import {AuthConext} from '../../auth/AuthConext';
import { useHistory } from "react-router-dom";
import { types } from '../../types/types';

export const NavBar = () => {

    const {user:{name},dispacth} = useContext(AuthConext)
    let history = useHistory();

    const handleLogout = () => {
        dispacth({
            type: types.logout
        })
        history.replace('/login')
    }

    return(
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <Link className="navbar-brand" to="/">Home</Link>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <NavLink className="nav-item nav-link" to="/Dc">Dc</NavLink>
                    <NavLink className="nav-item nav-link" to="/Marvel">Marvel</NavLink>
                </div>
            </div>
            <div className="nav navbar-nav navbar-right">
                <span className='nav-item nav-link text-info'>{name}</span>
                <button className="nav-item nav-link btn" onClick={handleLogout}>Logout </button>
            </div>
        </nav>
    )
}