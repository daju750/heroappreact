import React,{useContext} from 'react'
import { useHistory } from "react-router-dom";
import {AuthConext} from '../auth/AuthConext';
import { types } from '../types/types';

export const LoginScreen = () => {

    let history = useHistory();

    const {dispacth} = useContext(AuthConext)

    const lastPath = localStorage.getItem('lastPath')||'/'
    
    const handleLogin = () => {
        dispacth({
            type: types.login,
            payload:{
                name:'Fernando'
            }
        })
        history.replace(lastPath)
    }

    return(
        <div className='container mt-5'>
            <h1>Login</h1>
            <hr/>

            <button className='btn btn-primary' onClick={handleLogin}>Login</button>
        </div>
    )
}