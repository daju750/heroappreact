import React, {useEffect, useReducer} from 'react';
import {AuthConext} from './auth/AuthConext';
import {authReducer} from './auth/authReducer';
import {AppRouter} from './components/routers/AppRouter';

const init = () =>{
    return JSON.parse(localStorage.getItem('user'))||{logged:false}
}

export const HeroApp = () => {

    const[user,dispacth] = useReducer(authReducer,{},init)

    //Para Gagrar aunque recarge la pagina
    useEffect(()=>{
        localStorage.setItem('user',JSON.stringify(user))
    },[user])

    return (
        <>
            <AuthConext.Provider value={{user,dispacth}}>
            <AppRouter/>
            </AuthConext.Provider>
        </>
    )

}